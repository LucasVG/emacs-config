(fset 'sc
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("	Saccharomyces cerevisiae" 0 "%d")) arg)))
(global-set-key [24 11 49] 'sc)
(fset 'fsanfran
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([3 6 9 70 114 117 99 116 105 108 97 99 116 111 98 97 99 105 108 108 117 115 32 115 97 110 102 114 97 110 105 backspace 99 105 115 99 101 110 115 105 115] 0 "%d")) arg)))
(global-set-key [24 11 50] 'fsanfran)
(fset 'Llacsplac
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([3 6 9 76 46 32 108 97 99 116 105 115 right 32 115 112 46 32 3 6 9 108 97 99 116 105 115 right 32] 0 "%d")) arg)))
