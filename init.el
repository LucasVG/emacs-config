;;; package --- Summary:
(setq byte-compile-warnings '(cl-functions))
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(straight-use-package 'org)

(setq vc-follow-symlinks t)

;;; Commentary:
;;; Code:

(org-babel-load-file (concat user-emacs-directory "config.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-goodies/feed-source-column-width 40)
 '(safe-local-variable-values
   '((TeX-master . t)
     (eval TeX-run-style-hooks "commandes_cours")
     (eval TeX-run-style-hooks "packages_cours")
     (eval TeX-run-style-hooks "/home/drloiseau/.emacs.d/style/package")
     (eval TeX-run-style-hooks "auto/command")
     (eval TeX-run-style-hooks "command")
     (eval TeX-run-style-hooks "package")
     (TeX-master . dossier_scientifique\.tex)
     (TeX-master . Article_StabLevain-Farines\.tex)
     (TeX-Master . t)))
 '(warning-suppress-types '((comp))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
